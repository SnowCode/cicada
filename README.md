# Cicada 3301 toolbox
This tool box is made to solve the Cicada's enigamas faster.

## Install

```bash
git clone https://codeberg.org/SnowCode/cicada
cd cicada
bash install.sh
```

## Usage

| Command | Purpose |
| --- | --- |
| `checkada` | Knowing if a message comes from Cicada 3301 |
